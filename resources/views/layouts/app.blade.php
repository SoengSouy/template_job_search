
 <!DOCTYPE html>
 <html lang="en">
 
 <head>
    <title>soengsouy.com</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="#">
    <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">
    <!-- Favicon icon -->

    <link rel="icon" href="{{URL::to('assets/files/assets/images/favicon.ico')}}" type="image/x-icon">
    <!-- Google font--><link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="{{URL::to('assets/files/bower_components/bootstrap/css/bootstrap.min.css')}}">
    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css" href="{{URL::to('assets/files/assets/icon/themify-icons/themify-icons.css')}}">
    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="{{URL::to('assets/files/assets/icon/icofont/css/icofont.css')}}">
    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css" href={{URL::to('assets/files/assets/icon/themify-icons/themify-icons.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="{{URL::to('assets/files/assets/icon/font-awesome/css/font-awesome.min.css')}}">
    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="{{URL::to('assets/files/assets/icon/icofont/css/icofont.css')}}">
    <!-- feather Awesome -->
    <link rel="stylesheet" type="text/css" href="{{URL::to('assets/files/assets/icon/feather/css/feather.css')}}">

     <!-- Style.css -->
     <link rel="stylesheet" type="text/css" href="{{URL::to('assets/files/assets/css/style.css')}}">
 </head>
 
 
 <body class="fix-menu">
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="ball-scale">
            <div class="preloader6">
                <hr>
            </div>
        </div>
    </div>
    
     @yield('content')
 
     <!-- Warning Section Ends -->
     <!-- Required Jquery -->
     <script type="text/javascript" src="{{URL::to('assets/files/bower_components/jquery/js/jquery.min.js')}}"></script>
     <script type="text/javascript" src="{{URL::to('assets/files/bower_components/jquery-ui/js/jquery-ui.min.js')}}"></script>
     <script type="text/javascript" src="{{URL::to('assets/files/bower_components/popper.js/js/popper.min.js')}}"></script>
     <script type="text/javascript" src="{{URL::to('assets/files/bower_components/bootstrap/js/bootstrap.min.js')}}"></script>
     <!-- jquery slimscroll js -->
     <script type="text/javascript" src="{{URL::to('assets/files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js')}}"></script>
     <!-- modernizr js -->
     <script type="text/javascript" src="{{URL::to('assets/files/bower_components/modernizr/js/modernizr.js')}}"></script>
     <script type="text/javascript" src="{{URL::to('assets/files/bower_components/modernizr/js/css-scrollbars.js')}}"></script>
     <!-- i18next.min.js -->
     <script type="text/javascript" src="{{URL::to('assets/files/bower_components/i18next/js/i18next.min.js')}}"></script>
     <script type="text/javascript" src="{{URL::to('assets/files/bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js')}}"></script>
     <script type="text/javascript" src="{{URL::to('assets/files/bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js')}}"></script>
     <script type="text/javascript" src="{{URL::to('assets/files/bower_components/jquery-i18next/js/jquery-i18next.min.js')}}"></script>
     <script type="text/javascript" src="{{URL::to('assets/files/assets/js/common-pages.js')}}"></script>
 
 </body>
 </html>
 
 