
<!DOCTYPE html>
<html lang="en">

<head>
    <title>soengsouy.com</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="#">
    <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">
    <!-- Favicon icon -->
    <link rel="icon" href="{{URL::to('assets/files/assets/images/favicon.ico')}}" type="image/x-icon">
    <!--forms-wizard css-->
    <link rel="stylesheet" type="text/css" href="{{URL::to('assets/files/bower_components/jquery.steps/css/jquery.steps.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::to('assets/files/assets/icon/icofont/css/icofont.css')}}">
    
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="{{URL::to('assets/files/bower_components/bootstrap/css/bootstrap.min.css')}}">
    <!-- feather Awesome -->
    <link rel="stylesheet" type="text/css" href="{{URL::to('assets/files/assets/icon/feather/css/feather.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::to('assets/files/assets/icon/themify-icons/themify-icons.css')}}">

    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css" href="{{URL::to('assets/files/assets/icon/themify-icons/themify-icons.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::to('assets/files/assets/icon/feather/css/feather.css')}}">
    
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="{{URL::to('assets/files/assets/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::to('assets/files/assets/css/jquery.mCustomScrollbar.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::to('assets/files/assets/icon/font-awesome/css/font-awesome.min.css')}}">

    <!-- animation nifty modal window effects css -->
    <link rel="stylesheet" type="text/css" href="{{URL::to('assets/files/assets/css/component.css')}}">

    <!-- notify js Fremwork -->
    <link rel="stylesheet" type="text/css" href="{{URL::to('assets/files/bower_components/pnotify/css/pnotify.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::to('assets/files/bower_components/pnotify/css/pnotify.brighttheme.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::to('assets/files/bower_components/pnotify/css/pnotify.buttons.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::to('assets/files/bower_components/pnotify/css/pnotify.history.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::to('assets/files/bower_components/pnotify/css/pnotify.mobile.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::to('assets/files/assets/pages/pnotify/notify.css')}}">

    <!-- jpro forms css -->
    <link rel="stylesheet" type="text/css" href="{{URL::to('assets/files/assets/pages/j-pro/css/demo.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::to('assets/files/assets/pages/j-pro/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::to('assets/files/assets/pages/j-pro/css/j-pro-modern.css')}}">
    
    <!-- Data Table Css -->
    <link rel="stylesheet" type="text/css" href="{{URL::to('assets/files/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::to('assets/files/assets/pages/data-table/css/buttons.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::to('assets/files/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}">
    <!-- add multile form -->
    <script assets="{{URL::to('assets/files/assets/js/jquery-2.2.0-jquery.min.js')}}"></script>

</head>
<body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="ball-scale">
            <div class="preloader6">
                <hr>
            </div>
        </div>
    </div>


    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            <nav class="navbar header-navbar pcoded-header">
                <div class="navbar-wrapper">

                    <div class="navbar-logo">
                        <a class="mobile-menu" id="mobile-collapse" href="#!">
                            <i class="feather icon-menu"></i>
                        </a>
                        <a href="index-1.htm">
                            <img class="img-fluid" src="{{URL::to('assets/files/assets/images/logo.png')}}" alt="Theme-Logo">
                        </a>
                        <a class="mobile-options">
                            <i class="feather icon-more-horizontal"></i>
                        </a>
                    </div>

                    <div class="navbar-container container-fluid">
                        <ul class="nav-left">
                            <li class="header-search">
                                <div class="main-search morphsearch-search">
                                    <div class="input-group">
                                        <span class="input-group-addon search-close"><i class="feather icon-x"></i></span>
                                        <input type="text" class="form-control">
                                        <span class="input-group-addon search-btn"><i class="feather icon-search"></i></span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <a href="#!" onclick="javascript:toggleFullScreen()">
                                    <i class="feather icon-maximize full-screen"></i>
                                </a>
                            </li>
                        </ul>
                        <ul class="nav-right">
                            <li class="user-profile header-notification">
                                <div class="dropdown-primary dropdown">
                                    <div class="dropdown-toggle" data-toggle="dropdown">
                                        <img src="{{URL::to('assets/files/assets/images/user.png')}}" class="img-radius" alt="User-Profile-Image">
                                        {{-- <img src="{{ Auth::user()->avatar }}" class="img-radius" alt="User-Profile-Image"> --}}
                                        <span>{{ Auth::user()->name }}</span>
                                        <i class="feather icon-chevron-down"></i>
                                    </div>
                                    <ul class="show-notification profile-notification dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                        <li>
                                            <a href="{{ route('page/user/profile') }}">
                                                <i class="feather icon-user"></i> Profile
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('logout') }}">
                                                <i class="feather icon-log-out"></i> Logout
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            @yield('menu')
        </div>
    </div>

<!-- Required Jquery -->
<script type="text/javascript" src="{{URL::to('assets/files/bower_components/jquery/js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/files/bower_components/jquery-ui/js/jquery-ui.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/files/bower_components/popper.js/js/popper.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/files/bower_components/bootstrap/js/bootstrap.min.js')}}"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="{{URL::to('assets/files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js')}}"></script>
<!-- modernizr js -->
<script type="text/javascript" src="{{URL::to('assets/files/bower_components/modernizr/js/modernizr.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/files/bower_components/modernizr/js/css-scrollbars.js')}}"></script>
<!-- Google map js -->
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=true"></script>
<script type="text/javascript" src="{{URL::to('assets/files/assets/pages/google-maps/gmaps.js')}}"></script>

<!-- datatable js -->
<script src="{{URL::to('assets/files/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{URL::to('assets/files/bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{URL::to('assets/files/bower_components/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{URL::to('assets/files/bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>
<!-- jquery file upload js -->
<script src="{{URL::to('assets/files/assets/pages/jquery.filer/js/jquery.filer.min.js')}}"></script>
<script src="{{URL::to('assets/files/assets/pages/filer/custom-filer.js" type="text/javascript')}}"></script>
<script src="{{URL::to('assets/files/assets/pages/filer/jquery.fileuploads.init.js" type="text/javascrip')}}t"></script>
<!-- Model animation js -->
<script src="{{URL::to('assets/files/assets/js/classie.js')}}"></script>
<script src="{{URL::to('assets/files/assets/js/modalEffects.js')}}"></script>
<!-- product list js -->
<script type="text/javascript" src="{{URL::to('assets//files/assets/pages/product-list/product-list.js')}}"></script>
<!-- barrating js -->
<script type="text/javascript" src="{{URL::to('assets/files/bower_components/jquery-bar-rating/js/jquery.barrating.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/files/assets/pages/rating/rating.js')}}"></script>
<!-- slick js -->
<script type="text/javascript" src="{{URL::to('assets/files/bower_components/slick-carousel/js/slick.min.js')}}"></script>
<!-- product detail js -->
<script type="text/javascript" src="{{URL::to('assets/files/assets/pages/product-detail/product-detail.js')}}"></script>

<!--Forms - Wizard js-->
<script src="{{URL::to('assets/files/bower_components/jquery.cookie/js/jquery.cookie.js')}}"></script>
<script src="{{URL::to('assets/files/bower_components/jquery.steps/js/jquery.steps.js')}}"></script>
<script src="{{URL::to('assets/files/bower_components/jquery-validation/js/jquery.validate.js')}}"></script>
<!-- Validation js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
<script type="text/javascript" src="{{URL::to('assets/files/assets/pages/form-validation/validate.js')}}"></script>

<!-- amchart js -->
<script src="{{URL::to('assets/files/assets/pages/widget/amchart/amcharts.js')}}"></script>
<script src="{{URL::to('assets/files/assets/pages/widget/amchart/serial.js')}}"></script>
<script src="{{URL::to('assets/files/assets/pages/widget/amchart/light.js')}}"></script>
<!-- Chart js -->
<script type="text/javascript" src="{{URL::to('assets/files/bower_components/chart.js/js/Chart.js')}}"></script>

<!-- gauge js -->
<script src="{{URL::to('assets/files/assets/pages/widget/gauge/gauge.min.js')}}"></script>
<script src="{{URL::to('assets/files/assets/pages/widget/amchart/amcharts.js')}}"></script>
<script src="{{URL::to('assets/files/assets/pages/widget/amchart/serial.js')}}"></script>
<script src="{{URL::to('assets/files/assets/pages/widget/amchart/gauge.js')}}"></script>
<script src="{{URL::to('assets/files/assets/pages/widget/amchart/pie.js')}}"></script>
<script src="{{URL::to('assets/files/assets/pages/widget/amchart/light.js')}}"></script>/

<!-- Custom js -->
<script type="text/javascript" src="{{URL::to('assets/files/assets/js/SmoothScroll.js')}}"></script>
<script src="{{URL::to('assets/files/assets/js/pcoded.min.js')}}"></script>
<script src="{{URL::to('assets/files/assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<script src="{{URL::to('assets/files/assets/js/vartical-layout.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/files/assets/pages/dashboard/analytic-dashboard.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets/files/assets/js/script.js')}}"></script>
<!-- Custom js -->
<script src="{{URL::to('assets/files/assets/pages/forms-wizard-validation/form-wizard.js')}}"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-23581568-13');
</script>

@yield('script')

</body>
</html>