
@extends('layouts.app')
@section('content')
    <!-- Pre-loader end -->
    <section class="login-block">
        <!-- Container-fluid starts -->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <!-- Authentication card start -->
                    <form method="POST" action="{{ route('register') }}" class="md-float-material form-material">
                        @csrf
                        <div class="text-center">
                            <img src="{{URL::to('assets/files/assets/images/logo.png')}}" alt="logo.png">
                        </div>
                        <div class="auth-box card">
                            <div class="card-block">
                                <div class="row m-b-20">
                                    <div class="col-md-12">
                                        <h3 class="text-center txt-primary">Sign up</h3>
                                    </div>
                                </div>
                                <p class="text-muted text-center p-b-5">Sign up with your regular account</p>
                                <div class="form-group form-primary">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" autocomplete="name" autofocus  placeholder="Username">
                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    <span class="form-bar"></span>
                                </div>
                                <div class="form-group form-primary">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autocomplete="email" placeholder="Enter email">
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    <span class="form-bar"></span>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group form-primary">
                                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" autocomplete="new-password" placeholder="Enter password">
                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                            <span class="form-bar"></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group form-primary">
                                            <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm Password">
                                            <span class="form-bar"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row m-t-20">
                                    <div class="col-md-6">
                                        <a  href="{{ route('login.facebook') }}">
                                            <button type="button" class="btn btn-facebook m-b-20 btn-block btn-round">
                                                <i class="icofont icofont-social-facebook"></i>Facebook
                                            </button>
                                        </a>
                                    </div>
                                    <div class="col-md-6">
                                        <a  href="{{ route('login.google') }}">
                                            <button type="button" class="btn btn-twitter m-b-20 btn-block btn-round">
                                                <i class="fa fa-google"></i>Google
                                            </button>
                                        </a>
                                    </div>
                                </div>
                                
                                <div class="row m-t-25">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary btn-round btn-block waves-effect text-center m-b-20">Sign Up</button>
                                    </div>
                                </div>
                                <p class="text-inverse text-left">Have an account?<a href="{{route('login')}}"> <b class="f-w-600">Login </b></a>for free!</p>
                            </div>
                        </div>
                    </form>
                        <!-- end of form -->
                    </div>
                    <!-- Authentication card end -->
                </div>
                <!-- end of col-sm-12 -->
            </div>
            <!-- end of row -->
        <!-- end of container-fluid -->
    </section>
    <!-- Warning Section Starts -->
@endsection
