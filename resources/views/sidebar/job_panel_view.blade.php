<div class="pcoded-main-container">
    <div class="pcoded-wrapper">
        <nav class="pcoded-navbar">
            <div class="pcoded-inner-navbar main-menu">
                <div class="pcoded-navigatio-lavel">Menu</div>
                <ul class="pcoded-item pcoded-left-item">
                    <li class="active">
                        <a href="{{ route('home') }}">
                            <span class="pcoded-micon"><i class="feather icon-home"></i></span>
                            <span class="pcoded-mtext">Dashboard</span>
                        </a>
                    </li>
                    <li class="pcoded-hasmenu ">
                        <a href="javascript:void(0)">
                            <span class="pcoded-micon"><i class="feather icon-clipboard"></i></span>
                            <span class="pcoded-mtext">User Management</span>
                        </a>
                        <ul class="pcoded-submenu">
                            <li class="">
                                <a href="dashboard-crm.htm">
                                    <span class="pcoded-mtext">Admin Page</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="pcoded-hasmenu pcoded-trigger" dropdown-icon="style1" subitem-icon="style1">
                        <a href="javascript:void(0)">
                            <span class="pcoded-micon"><i class="feather icon-award"></i></span>
                            <span class="pcoded-mtext">Job Search</span>
                            <span class="pcoded-badge label label-danger">NEW</span>
                        </a>
                        <ul class="pcoded-submenu">
                            <li class="">
                                <a href="{{ route('form/card_view/new') }}">
                                    <span class="pcoded-mtext">Card View</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="{{ route('form/job_detail/new') }}">
                                    <span class="pcoded-mtext">Job Detailed</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="{{ route('form/job_find/new') }}">
                                    <span class="pcoded-mtext">Job Find</span>
                                </a>
                            </li>
                            <li class="active">
                                <a href="{{ route('form/job_panel_view/new') }}">
                                    <span class="pcoded-mtext">Job Panel View</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>