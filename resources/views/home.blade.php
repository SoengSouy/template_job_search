
@extends('layouts.master')
@section('menu')
@include('sidebar.dashboard')
<div class="pcoded-content">
    <div class="pcoded-inner-content">
        <div class="main-body">
            <div class="page-wrapper">
                <div class="page-body">
                    <div class="row">
                        <!-- task, page, download counter  start -->
                        <div class="col-xl-3 col-md-6">
                            <div class="card">
                                <div class="card-block">
                                    <div class="row align-items-center">
                                        <div class="col-8">
                                            <h4 class="text-c-yellow f-w-600">$30200</h4>
                                            <h6 class="text-muted m-b-0">All Earnings</h6>
                                        </div>
                                        <div class="col-4 text-right">
                                            <i class="feather icon-bar-chart f-28"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer bg-c-yellow">
                                    <div class="row align-items-center">
                                        <div class="col-9">
                                            <p class="text-white m-b-0">% change</p>
                                        </div>
                                        <div class="col-3 text-right">
                                            <i class="feather icon-trending-up text-white f-16"></i>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6">
                            <div class="card">
                                <div class="card-block">
                                    <div class="row align-items-center">
                                        <div class="col-8">
                                            <h4 class="text-c-green f-w-600">290+</h4>
                                            <h6 class="text-muted m-b-0">Page Views</h6>
                                        </div>
                                        <div class="col-4 text-right">
                                            <i class="feather icon-file-text f-28"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer bg-c-green">
                                    <div class="row align-items-center">
                                        <div class="col-9">
                                            <p class="text-white m-b-0">% change</p>
                                        </div>
                                        <div class="col-3 text-right">
                                            <i class="feather icon-trending-up text-white f-16"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6">
                            <div class="card">
                                <div class="card-block">
                                    <div class="row align-items-center">
                                        <div class="col-8">
                                            <h4 class="text-c-pink f-w-600">145</h4>
                                            <h6 class="text-muted m-b-0">Task Completed</h6>
                                        </div>
                                        <div class="col-4 text-right">
                                            <i class="feather icon-calendar f-28"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer bg-c-pink">
                                    <div class="row align-items-center">
                                        <div class="col-9">
                                            <p class="text-white m-b-0">% change</p>
                                        </div>
                                        <div class="col-3 text-right">
                                            <i class="feather icon-trending-up text-white f-16"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6">
                            <div class="card">
                                <div class="card-block">
                                    <div class="row align-items-center">
                                        <div class="col-8">
                                            <h4 class="text-c-blue f-w-600">500</h4>
                                            <h6 class="text-muted m-b-0">Downloads</h6>
                                        </div>
                                        <div class="col-4 text-right">
                                            <i class="feather icon-download f-28"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer bg-c-blue">
                                    <div class="row align-items-center">
                                        <div class="col-9">
                                            <p class="text-white m-b-0">% change</p>
                                        </div>
                                        <div class="col-3 text-right">
                                            <i class="feather icon-trending-up text-white f-16"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- social start -->
                        <div class="col-xl-4 col-md-12">
                            <div class="card quater-card">
                                <div class="card-block">
                                    <h6 class="text-muted m-b-20">This Quarter</h6>
                                    <h4>$3,9452.50</h4>
                                    <p class="text-muted">$3,9452.50</p>
                                    <h5 class="m-t-30">87</h5>
                                    <p class="text-muted">Online Revenue<span class="f-right">80%</span></p>
                                    <div class="progress">
                                        <div class="progress-bar bg-simple-c-pink" style="width: 80%"></div>
                                    </div>
                                    <h5 class="m-t-30">68</h5>
                                    <p class="text-muted">Offline Revenue<span class="f-right">50%</span></p>
                                    <div class="progress">
                                        <div class="progress-bar bg-simple-c-yellow" style="width: 50%"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-8 col-md-12">
                            <div class="card social-network">
                                <div class="card-header">
                                    <h5>Social Network</h5>
                                </div>
                                <div class="card-block">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <img src="{{URL::to('assets/files/assets/images/widget/icon-1.png')}}" alt=" " class="img-responsive p-b-20">
                                            <div class="row">
                                                <div class="col-5">
                                                    <p class="text-muted m-b-5">Views :</p>
                                                </div>
                                                <div class="col-7">
                                                    <p class="m-b-5 f-w-400">545,721</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-5">
                                                    <p class="text-muted m-b-5">Comments :</p>
                                                </div>
                                                <div class="col-7">
                                                    <p class="m-b-5 f-w-400">2,256</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-5">
                                                    <p class="text-muted m-b-5">Likes :</p>
                                                </div>
                                                <div class="col-7">
                                                    <p class="m-b-5 f-w-400">4,129</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-5">
                                                    <p class="text-muted m-b-5">Subscribe :</p>
                                                </div>
                                                <div class="col-7">
                                                    <p class="m-b-5 f-w-400">3,451,945</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <img src="{{URL::to('assets/files/assets/images/widget/icon-2.png')}}" alt=" " class="img-responsive p-b-20">
                                            <div class="row">
                                                <div class="col-5">
                                                    <p class="text-muted m-b-5">Engagement :</p>
                                                </div>
                                                <div class="col-7">
                                                    <p class="m-b-5 f-w-400">1,543</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-5">
                                                    <p class="text-muted m-b-5">Shares :</p>
                                                </div>
                                                <div class="col-7">
                                                    <p class="m-b-5 f-w-400">846</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-5">
                                                    <p class="text-muted m-b-5">Likes :</p>
                                                </div>
                                                <div class="col-7">
                                                    <p class="m-b-5 f-w-400">569</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-5">
                                                    <p class="text-muted m-b-5">Comments :</p>
                                                </div>
                                                <div class="col-7">
                                                    <p class="m-b-5 f-w-400">156</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6 m-t-0">
                                            <img src="{{URL::to('assets/files/assets/images/widget/icon-3.png')}}" alt=" " class="img-responsive p-b-10 p-t-10">
                                            <div class="row">
                                                <div class="col-5">
                                                    <p class="text-muted m-b-5">Tweets :</p>
                                                </div>
                                                <div class="col-7">
                                                    <p class="m-b-5 f-w-400">103,576</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 m-t-0">
                                            <img src="{{URL::to('assets/files/assets/images/widget/icon-4.png')}}" alt=" " class="img-responsive p-b-10 p-t-10">
                                            <div class="row">
                                                <div class="col-5">
                                                    <p class="text-muted m-b-5">Tweets :</p>
                                                </div>
                                                <div class="col-7">
                                                    <p class="m-b-5 f-w-400">103,576</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- social end -->

                        <div class="col-xl-4 col-md-6">
                            <div class="card"><iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; inset: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
                                <div class="card-header">
                                    <h5>Total Leads</h5>
                                    <div class="card-header-right">
                                        <ul class="list-unstyled card-option">
                                            <li><i class="fa fa fa-wrench open-card-option"></i></li>
                                            <li><i class="fa fa-window-maximize full-card"></i></li>
                                            <li><i class="fa fa-minus minimize-card"></i></li>
                                            <li><i class="fa fa-refresh reload-card"></i></li>
                                            <li><i class="fa fa-trash close-card"></i></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <p class="text-c-green f-w-500"><i class="feather icon-chevrons-up m-r-5"></i> 18% High than last month</p>
                                    <div class="row">
                                        <div class="col-4 b-r-default">
                                            <p class="text-muted m-b-5">Overall</p>
                                            <h5>76.12%</h5>
                                        </div>
                                        <div class="col-4 b-r-default">
                                            <p class="text-muted m-b-5">Monthly</p>
                                            <h5>16.40%</h5>
                                        </div>
                                        <div class="col-4">
                                            <p class="text-muted m-b-5">Day</p>
                                            <h5>4.56%</h5>
                                        </div>
                                    </div>
                                </div>
                                <canvas id="tot-lead" height="150" width="508" style="display: block;"></canvas>
                            </div>
                        </div>

                        <div class="col-xl-4 col-md-6">
                            <div class="card"><iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; inset: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
                                <div class="card-header">
                                    <h5>Total Vendors</h5>
                                    <div class="card-header-right">
                                        <ul class="list-unstyled card-option">
                                            <li><i class="fa fa fa-wrench open-card-option"></i></li>
                                            <li><i class="fa fa-window-maximize full-card"></i></li>
                                            <li><i class="fa fa-minus minimize-card"></i></li>
                                            <li><i class="fa fa-refresh reload-card"></i></li>
                                            <li><i class="fa fa-trash close-card"></i></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <p class="text-c-pink f-w-500"><i class="feather icon-chevrons-down m-r-15"></i> 24% High than last month</p>
                                    <div class="row">
                                        <div class="col-4 b-r-default">
                                            <p class="text-muted m-b-5">Overall</p>
                                            <h5>68.52%</h5>
                                        </div>
                                        <div class="col-4 b-r-default">
                                            <p class="text-muted m-b-5">Monthly</p>
                                            <h5>28.90%</h5>
                                        </div>
                                        <div class="col-4">
                                            <p class="text-muted m-b-5">Day</p>
                                            <h5>13.50%</h5>
                                        </div>
                                    </div>
                                </div>
                                <canvas id="tot-vendor" height="150" width="508" style="display: block;"></canvas>
                            </div>
                        </div>

                        <div class="col-xl-4 col-md-12">
                            <div class="card"><iframe class="chartjs-hidden-iframe" tabindex="-1" style="display: block; overflow: hidden; border: 0px; margin: 0px; inset: 0px; height: 100%; width: 100%; position: absolute; pointer-events: none; z-index: -1;"></iframe>
                                <div class="card-header">
                                    <h5>Invoice Generate</h5>
                                    <div class="card-header-right">
                                        <ul class="list-unstyled card-option">
                                            <li><i class="fa fa fa-wrench open-card-option"></i></li>
                                            <li><i class="fa fa-window-maximize full-card"></i></li>
                                            <li><i class="fa fa-minus minimize-card"></i></li>
                                            <li><i class="fa fa-refresh reload-card"></i></li>
                                            <li><i class="fa fa-trash close-card"></i></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <p class="text-c-green f-w-500"><i class="feather icon-chevrons-up m-r-15"></i> 20% High than last month</p>
                                    <div class="row">
                                        <div class="col-4 b-r-default">
                                            <p class="text-muted m-b-5">Overall</p>
                                            <h5>68.52%</h5>
                                        </div>
                                        <div class="col-4 b-r-default">
                                            <p class="text-muted m-b-5">Monthly</p>
                                            <h5>28.90%</h5>
                                        </div>
                                        <div class="col-4">
                                            <p class="text-muted m-b-5">Day</p>
                                            <h5>13.50%</h5>
                                        </div>
                                    </div>
                                </div>
                                <canvas id="invoice-gen" height="150" width="508" style="display: block;"></canvas>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
              
@endsection