
@extends('layouts.master')
@section('menu')
    @include('sidebar.product_list')
    <div class="pcoded-content">
        <div class="pcoded-inner-content">
            <!-- Main-body start -->
            <div class="main-body">
                <div class="page-wrapper">
                    <!-- Page-header start -->
                    <div class="page-header">
                        <div class="row align-items-end">
                            <div class="col-lg-8">
                                <div class="page-header-title">
                                    <div class="d-inline">
                                        <h4>Product List</h4>
                                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="page-header-breadcrumb">
                                    <ul class="breadcrumb-title">
                                        <li class="breadcrumb-item">
                                            <a href="{{ route('home') }}"> <i class="feather icon-home"></i> </a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="#!">E-Commerce</a>
                                        </li>
                                        <li class="breadcrumb-item"><a href="#!">Product List</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Page-header end -->

                        <!-- Page body start -->
                        <div class="page-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <!-- Product list card start -->
                                    <div class="card">
                                        <div class="card-header">
                                            <h5>Product List</h5>
                                            <button type="button" class="btn btn-primary waves-effect waves-light f-right d-inline-block md-trigger" data-modal="modal-13"> <i class="icofont icofont-plus m-r-5"></i> Add Product
                                            </button>
                                        </div>
                                        
                                        <div class="card-block">
                                            @if ($message = Session::get('success'))
                                                <div class="alert alert-success">
                                                    <strong>{{ $message }}</strong>
                                                </div>
                                            @endif
                                
                                            @if (count($errors) > 0)
                                                <div class="alert alert-danger">
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                                            <div class="table-responsive">
                                                <div class="table-content">
                                                    <div class="project-table">
                                                        <table id="e-product-list" class="table table-striped dt-responsive nowrap">
                                                            <thead>
                                                                <tr>
                                                                    <th>No</th>
                                                                    <th>Image</th>
                                                                    <th>Product Name</th>
                                                                    <th>Amount</th>
                                                                    <th>Stock</th>
                                                                    <th>Action</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach($product as $products)
                                                                    <tr>
                                                                        <td class="id">
                                                                            <label class="text-success">{{ $products->id }}</label>
                                                                        </td>
                                                                        <td class="file">
                                                                            <h6 hidden>{{ $products->name_photo }}</h6>
                                                                            <img src="{{URL::to('assets/files/assets/images/product-list/'.$products->name_photo)}}" class="img-fluid" alt="tbl">
                                                                        </td>
                                                                        <td class="product_name">
                                                                            <h6>{{ $products->product_name }}</h6>
                                                                        </td>
                                                                        <td class="amount">{{ $products->amount }}</td>
                                                                        @if($products->stock =='In Stock')
                                                                        <td class="stock">
                                                                            <label class="text-success">{{ $products->stock }}</label>
                                                                        </td>
                                                                        @endif
                                                                        @if($products->stock =='Law Stock')
                                                                        <td class="stock">
                                                                            <label class="text-warning">{{ $products->stock }}</label>
                                                                        </td>
                                                                        @endif
                                                                        @if($products->stock =='Out of Stock')
                                                                        <td class="stock">
                                                                            <label class="text-danger">{{ $products->stock }}</label>
                                                                        </td>
                                                                        @endif
                                                                        <td class="action-icon">
                                                                            <a href="#!" class="m-r-15 text-muted d-inline-block md-trigger productUpdate" data-modal="update"><i class="icofont icofont-ui-edit"></i></a>
                                                                            <a href="#!" class="text-muted" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><i class="icofont icofont-delete-alt"></i></a>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Product list card end -->
                                </div>
                            </div>
                            <!-- Add Contact Start Model start-->
                            <div class="md-modal md-effect-13 addcontact" id="modal-13">
                                <div class="md-content">
                                    <h3 class="f-26">Add Product</h3>
                                    <div>
                                        <form id="" action="{{ route('product/list/save') }}" method="POST" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <div class="input-group">
                                                <input type="file" id="file"name="file" class="form-control" >
                                                <span class="input-group-addon btn btn-primary">Chooese File</span>
                                            </div>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                                                <input type="text" id="product_name"name="product_name" class="form-control" placeholder="Prodcut Name">
                                            </div>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                                                <input type="text" id="amount"name="amount" class="form-control" placeholder="Amount">

                                            </div>
                                            <div class="input-group">
                                                <select id="stock" name="stock" class="form-control ">
                                                    <option value="">---- Select Stock ----</option>
                                                    <option value="In Stock">In Stock</option>
                                                    <option value="Out of Stock">Out of Stock</option>
                                                    <option value="Law Stock">Law Stock</option>
                                                </select>
                                            </div>
                                            <div class="text-center">
                                                <button type="submit" class="btn btn-primary waves-effect m-r-20 f-w-600 d-inline-block save_btn">Save</button>
                                                <button type="button" class="btn btn-primary waves-effect m-r-20 f-w-600 md-close d-inline-block close_btn">Close</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="md-overlay"></div>
                            <!-- Add Contact Ends Model end-->

                            <!-- Update Contact Start Model start-->
                            <div class="md-modal md-effect-13 addcontact" id="update">
                                <div class="md-content">
                                    <h3 class="f-26">Update Product</h3>
                                    <div>
                                        <form id="" action="{{ route('product/list/update') }}" method="POST" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <input type="hidden" class="form-control" id="v_id" name="id" value=""/>
                                            <input type="hidden" id="e_file"name="file" class="form-control" value="" />

                                            <div class="input-group">
                                                <input type="file" id="file"name="file" class="form-control">
                                                <span class="input-group-addon btn btn-primary">Chooese File</span>
                                            </div>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                                                <input type="text" id="e_product_name"name="product_name" class="form-control" value="" />
                                            </div>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="icofont icofont-user"></i></span>
                                                <input type="text" id="e_amount"name="amount" class="form-control" value="" />
                                            </div>
                                            <div class="input-group">
                                                <input type="text" id="e_stock"name="stock" class="form-control" value="" />
                                            </div>
                                            <div class="text-center">
                                                <button type="submit" class="btn btn-primary waves-effect m-r-20 f-w-600 d-inline-block save_btn">Update</button>
                                                <button type="button" class="btn btn-primary waves-effect m-r-20 f-w-600 md-close d-inline-block close_btn">Close</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="md-overlay"></div>
                            <!-- Update Contact Ends Model end-->
                        </div>
                        <!-- Page body end -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@section('script')
    {{-- update js --}}
<script>
    $(document).on('click','.productUpdate',function()
    {
        var _this = $(this).parents('tr');
        $('#v_id').val(_this.find('.id').text());
        $('#e_file').val(_this.find('.file').text());
        $('#e_product_name').val(_this.find('.product_name').text());
        $('#e_amount').val(_this.find('.amount').text());
        $('#e_stock').val(_this.find('.stock').text());
        $('#e_role_name').val(_this.find('.role_name').text());
    });
</script>
@endsection   
@endsection


