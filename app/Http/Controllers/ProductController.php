<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class ProductController extends Controller
{
    // Product
    public function product()
    {
        return view('E-Commerce.product');
    }
    // productList
    public function productList()
    {
        $product = Product::all();
        return view('E-Commerce.product_list',compact('product'));
    }
    // save
    public function productListSave(Request $request)
    {
        $product_name = $request->product_name;
        $amount      = $request->amount;
        $stock       = $request->stock;
       
        if($request->hasfile('file'))
        {
            $image = $request->file('file');
            $filename = $image->getClientOriginalName();
            $image->move(public_path('/assets/files/assets/images/product-list/'), $filename);
            $name = $request->file('file')->getClientOriginalName();
      
              $fileModal = new Product();
              $fileModal->name_photo = $name;
              $fileModal->product_name = $product_name;
              $fileModal->amount = $amount;
              $fileModal->stock = $stock;
              
              $fileModal->save();
      
             return back()->with('success', 'Insert has successfully uploaded!');
          }

    }
    // update 
    public function productListUpdate(Request $request)
    {
        
        $id = $request->id;
        $product_name = $request->product_name;
        $amount      = $request->amount;
        $stock       = $request->stock;
    
        $image = $request->file('file');
        $filename = $image->getClientOriginalName();
        $image->move(public_path('/assets/files/assets/images/product-list/'), $filename);
        $name = $request->file('file')->getClientOriginalName();
  
        $fileModal = new Product();
        $fileModal->id = $id;
        $fileModal->name_photo = $name;
        $fileModal->product_name = $product_name;
        $fileModal->amount = $amount;
        $fileModal->stock = $stock;
        $fileModal->update();
    
        return back()->with('success', 'Insert has successfully uploaded!');
    }
    // productDetail
    public function productDetail()
    {
        return view('E-Commerce.product_detail');
    }
    // productEdit
    public function productEdit()
    {
        return view('E-Commerce.product_edit');
    }
    // productCard
    public function productCard()
    {
        return view('E-Commerce.product_card');
    }
    // creditCard
    public function creditCard()
    {
        return view('E-Commerce.credit_card_form');
    }
}
