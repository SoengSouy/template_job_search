<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class JobSearchController extends Controller
{
    // view page
    public function jobFind()
    {
        return view('Job_Search.job_find');
    }
    // card view
    public function cardView()
    {
        return view('Job_Search.card_view');
    }
    // job detail
    public function jobDetail()
    {
        return view('Job_Search.job_detail');
    }
    // job panel view
    public function jobPanelView()
    {
        return view('Job_Search.job_panel_view');
    }
}
